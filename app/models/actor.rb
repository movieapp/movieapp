class Actor < ActiveRecord::Base

has_many :parts 
has_many :movies, through: :parts
    
# has_many :friends # Friend(actor_id, name)

# Has-many relationship without join model
# has_many :movies (and the inverse on Actor model)
# Create table called actor_movies (actor_id, movie_id)    
    
# Apple Orange => apple_oranges (apple_id, orange_id)
# Car Boat => boat_cars (car_id, boat_id)

end
